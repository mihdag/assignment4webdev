document.addEventListener('DOMContentLoaded', (event) => {
    (function() {
        const EpicApplication = {
            imagesData: [],
            imageType: '',
            dataCache: {
                'natural': null,
                'enhanced': null,
                'aerosol': null,
                'cloud': null
            },
            imageCache: {
                'natural': new Map(),
                'enhanced': new Map(),
                'aerosol': new Map(),
                'cloud': new Map()
            }
        };

        setMaxDate();

        document.getElementById('type').addEventListener('change', setMaxDate);

        document.getElementById('request_form').addEventListener('submit', function(event) {
            event.preventDefault();
            EpicApplication.imageType = document.getElementById('type').value;
            const date = document.getElementById('date').value;
            const url = `https://epic.gsfc.nasa.gov/api/${EpicApplication.imageType}/date/${date}`;

            if (EpicApplication.imageCache[EpicApplication.imageType].has(date)) {
                EpicApplication.imagesData = EpicApplication.imageCache[EpicApplication.imageType].get(date);
                populateImageMenu(EpicApplication.imagesData);
            } else {
                fetch(url)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error('Network response was not ok');
                        }
                        return response.json();
                    })
                    .then(data => {
                        EpicApplication.imagesData = data;
                        EpicApplication.imageCache[EpicApplication.imageType].set(date, EpicApplication.imagesData);
                        populateImageMenu(EpicApplication.imagesData);
                    })
                    .catch(error => console.error('Error:', error));
            }
        });

        function populateImageMenu(images) {
            const imageMenu = document.getElementById('image-menu');
            imageMenu.innerHTML = '';
            const listItemTemplate = document.createElement('li');
            images.forEach((image, index) => {
                const listItem = listItemTemplate.cloneNode();
                listItem.setAttribute('data-image-list-index', index);
                listItem.textContent = image.date;
                imageMenu.appendChild(listItem);
            });
        }

        function setMaxDate() {
            const imageType = document.getElementById('type').value;
            const today = new Date();
            let maxDate = new Date(today);
            let defaultDate = new Date(today);
        
            switch (imageType) {
                case 'natural':
                    maxDate.setDate(today.getDate() - 1);
                    defaultDate.setDate(today.getDate() - 1);
                    break;
                case 'enhanced':
                case 'aerosol':
                    maxDate.setDate(today.getDate() - 2);
                    defaultDate.setDate(today.getDate() - 2);
                    break;
                case 'cloud':
                    maxDate.setDate(today.getDate() - 13);
                    defaultDate.setDate(today.getDate() - 13);
                    break;
            }
        
            const formattedMaxDate = maxDate.toISOString().split('T')[0];
            const formattedDefaultDate = defaultDate.toISOString().split('T')[0];
            document.getElementById('date').setAttribute('max', formattedMaxDate);
            document.getElementById('date').value = formattedDefaultDate;
        
            EpicApplication.dataCache[imageType] = formattedMaxDate;
        }

        document.getElementById('image-menu').addEventListener('click', function(event) {
            if (event.target.tagName === 'LI') {
                const index = event.target.getAttribute('data-image-list-index');
                const image = EpicApplication.imagesData[index];
                const imageUrl = `https://epic.gsfc.nasa.gov/archive/${EpicApplication.imageType}/${image.date.slice(0, 4)}/${image.date.slice(5, 7)}/${image.date.slice(8, 10)}/jpg/${image.image}.jpg`;
                document.getElementById('earth-image').src = imageUrl;
                document.getElementById('earth-image-date').textContent = image.date;
                document.getElementById('earth-image-title').textContent = image.title;
                document.getElementById('earth-image-caption').textContent = image.caption;
            }
        });
    })();
});